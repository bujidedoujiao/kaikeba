const urlLib = require('url');
const http = require('http');
const https = require('https');
const pathLib = require('path');
const fs = require('fs');

let url = 'http://www.taobao.com'

let requestUrl = (url, headers) => {
    let urlObj = urlLib.parse(url);
    let httpMod = null
    if (urlObj.protocol === 'http:') {
        httpMod = http
    } else if (urlObj.protocol === 'https:') {
        httpMod = https
    } else {
        throw new Error('url 不是合法的网址')
    }
    return new Promise((resolve, reject) => {
        httpMod.request({
            host: urlObj.host,
            path: urlObj.path,
            headers: headers
        }, res => {
            const { statusCode } = res
            if (
                (statusCode >= 200 && statusCode < 300) ||
                statusCode === 304
            ) {
                let arr = []
                res.on('data', data => arr.push(data))
                res.on('end', () => {
                    let buffer = Buffer.concat(arr)
                    resolve({
                        statusCode,
                        body: buffer,
                        headers: res.headers
                    })
                })
            } else if (statusCode === 301 || statusCode === 302) {
                console.log(res.headers)
                resolve({
                    statusCode,
                    body: null,
                    headers: res.headers
                })
            } else {
                console.log('fail', statusCode)
                reject({
                    statusCode,
                    body: null,
                    headers: res.headers
                })
            }
        })
    })
}

let spider = async () => {
    let url = 'http://baidu.com'
    let result = null
    try {
        result = await requestUrl(url)
    } catch (error) {
        console.log(er)
    }
    console.log(result)
}